#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

int foo(const char *whoami)
{
        printf("I am a %s.  My pid is:%d  my ppid is %d\n",
                        whoami, getpid(), getppid() );
        return 1;
}

int main(void)
{
        int size= 1024;
        int i=0;
        int status=0;

        int x = 5;

        printf("Creating %d children\n", size);
        foo("parent");
        for(i=0;i<size;i++)
        {
                pid_t pid=fork();

                if (pid==0) /* only execute this if child */
                {
                        foo("child");
                        for(i=0; i< 5; i++)
                        {
                                x++;
                                printf("x value in for: %d ", x);
                                // write_dat_file_float("salida_prueba.raw", realVector, size);
                        }

                        exit(0);

                }
                wait(&status);  /* only the parent waits */
        }

        x = 100 * x;
        printf("x value in main: %d ", x);


        return 0;
}
