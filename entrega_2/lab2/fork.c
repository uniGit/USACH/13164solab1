#include <unistd.h> //Para utilizar fork(), pipes(), entre otros
#include <stdio.h> //Funciones de entrada y salida como printf
#include <stdlib.h> //Asignación de memoria, atoi, etc.
#include <string.h> 
#include <sys/wait.h> //Define las constantes simbólicas para usar con waitpid(), wait() por ejemplo
#include <sys/types.h> //define varios tipos de datos como pid_t

#define LECTURA 0
#define ESCRITURA 1


int main(int argc, char *argv[]) {
	int valorFork = fork(); //crea proceso
	int status;
	int *puntero; //variable local
	puntero = malloc(sizeof(int));
	if(valorFork == 0){ //hijo
		printf("el valor que retorna fork es: %i\n", valorFork);
		printf("al parecer soy el hijo y mi pid es: %i\n" , getpid());
		printf("mi padre debería ser el que tiene pid: %i\n", getppid() );
	}
	else{ //padre
		printf("el valor que retorna fork es: %i\n", valorFork);
		printf("al parecer soy el padre y mi pid es: %i\n" , getpid());
		wait(&status);
	}
}


