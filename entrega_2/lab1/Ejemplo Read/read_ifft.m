N = 1024;
%fv = fopen('salida.raw', 'r');
fv = fopen('demo.raw', 'r');
s = fread(fv, 'float');
s = reshape(s, 2, N*N);
s = s';
y = complex(s(:,1), s(:,2));

m = real(y);
%m = reshape(y, N, N);
m = reshape(m, N, N);
%m = m';
figure; imagesc(m); axis('off'); axis('square'); title('Dirty image'); colormap(hot)