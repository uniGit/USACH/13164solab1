#include <stdio.h>
#include <stdlib.h>
void read_dat_file_float(const char* fname, float *out, int len) {
    //open file in mode binary
    FILE* fid = fopen(fname, "rb");
    if (fid == NULL) {
        printf("ERROR in read_dat_file_float(): could not read %s\n", fname);
        exit(0);
    }
    //float* out = (float*) calloc(len, sizeof(float));

    fread(out, sizeof(float), len, fid);
    fclose(fid);
}

void write_dat_file_float(const char* fname, float* arr, int len){



    FILE* fid = fopen(fname, "wb");
    if (fid == NULL) return;
    fwrite(arr, sizeof(float), len, fid);
    fclose(fid);
}


int main(){
	// char nameFile[20] = "ifft_HLTau.raw";
	char nameFile[20] = "demo.raw";
	char nameFileOut[25] = "demo_salida2.raw";
	int size = 1024;
	int lenComplex = 2*size*size;
	int len = size*size;
	printf("antes de leer\n");
	float *Visibilidades = (float*)malloc(sizeof(float)*lenComplex);

	float *VisibilidadesOut = (float*)malloc(sizeof(float)*lenComplex);
	
	read_dat_file_float(nameFile, Visibilidades, lenComplex); // Leer archivo

	// Conversión aquí, falta lógica binario

	write_dat_file_float(nameFileOut, Visibilidades, lenComplex); // Escribir archivo

	printf("despues de escribir\n");
	free(VisibilidadesOut);
	free(Visibilidades);
	return 0;
}